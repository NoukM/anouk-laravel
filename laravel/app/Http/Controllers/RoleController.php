<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $roleList = Role::all();
        return View('roles.index',array('roleList' => $roleList));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roleList = Role::all();
        return View('roles.create',array('roleList' => $roleList));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role = new Role;
        $role->Name =  $request->get('Name');
      $role->save();
      
      return redirect('/roles/create');
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    $role = Role::find($id);
     $roleList = Role::all();
      return view('roles.show', array('role' => $role,'roleList' => $roleList));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
         $roleList = Role::all();

        return View('roles.edit',array('role' => $role,'roleList' => $roleList));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role = Role::where('Id', $id)
        ->update(['Name' => $request->get('Name')]);
        
        
        
        return redirect('/roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    $result=Role::where('Id',$id)->delete();
      
   
      $roleList = Role::all();

        return View('roles.index',array('roleList' => $roleList));
    }
}
