<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $countryList = Country::all();
        return View('countries.index',array('countryList' => $countryList));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countryList = Country::all();
        return View('countries.create',array('countryList' => $countryList));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $country = new Country;
        $country->Name =  $request->get('Name');
        $country->Code =  $request->get('Code');
      $country->save();
      
      return redirect('/countries/create');
      
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    $country = Country::find($id);
     $countryList = Country::all();
      return view('countries.show', array('country' => $country,'countryList' => $countryList));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country = Country::find($id);
         $countryList = Country::all();

        return View('countries.edit',array('country' => $country,'countryList' => $countryList));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $country = Country::where('Id', $id)
        ->update(['Name' => $request->get('Name'),'Code' => $request->get('Code')]);
        
        
        
        return redirect('/countries');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
     
      
    $result=Country::where('Id',$id)->delete();
      
   
      $countryList = Country::all();

        return View('countries.index',array('countryList' => $countryList));
    }
}
