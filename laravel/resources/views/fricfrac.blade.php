<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Beheer Fric-Frac</title>
</head>
<body>
    <header>
        <h1>Fric-Frac</h1>
    </header>
    <main>
        <section>
            <a href="Person/Index.php" class="tile" id=""><span>Persoon</span></a>
            <a href="{{route('countries')}}" class="tile" id=""><span>Land</span></a>
            <a class="tile" id=""></a>
            <a class="tile" id=""></a>
            <a href="{{routes('roles')}}" class="tile" id=""><span>Role</span></a>
            <a href="User/Index.php" class="tile" id=""><span>Gebruiker</span></a>
            <a class="tile" id=""></a>
            <a href="Event/Index.php" class="tile" id=""><span>Event</span></a>
            <a href="EventCategory/Index.php" class="tile" id=""><span>Event Categorie</span></a>
            <a href="EventTopic/Index.php" class="tile" id=""><span>Event Topic</span></a>
            <a class="tile" id=""></a>
        </section>
    </main>
</body>
</html>