    <main>
    <article>
         <nav class="NavReadingOne">
            <h1>Rol</h1>
            <div>
                <a href="index.php">Annuleren</a>
                <button type="submit" name="uc" value="update" form="form">Update</button>
            </div>
            </nav>
            
            
      <form  id="form" method="post" action="{{route('roles.update', $role->Id)}}">
          @method('PATCH')
          @csrf
         <div>
            <label for="Id">Ids</label>
            <input type="text" name="Id" id="Id" value={{ $role->Id }} readonly>
        </div>
        <div>
            <label for="Name">Naam</label>
            <input type="text" name="Name" id="Name" value={{ $role->Name }} >
        </div>

               
    </form>
        <div id="feedback"></div>

    </article>
     @include('roles.readingAll') 
</main>
    