<main>
    <article>

          <nav class="NavReadingOne">
            <h1>Land</h1>
            <div>
            <a href="{{route('countries.index')}}">Annuleren</a>
            <a href="{{route('countries.edit', ['id' => ($country->Id)] )}}">Update</a>
            <a href="{{route('countries.delete', ['id' => ($country->Id)] )}}">Delete</a>
            <a href="{{route('countries.create')}}">Insert</a>
            </div>
            </nav>
    
      <form>
         <div>
            <label for="Id">Id</label>
            <input type="text" name="Id" id="Id" value="{{ $country->Id }}" readonly>
        </div>
        <div>
            <label for="Name">Naam</label>
            <input type="text" name="Name" id="Name" value="{{ $country->Name }}" readonly>
        </div>
          <div>
            <label for="Code">Code</label>
            <input type="text" name="Code" id="Code" value="{{ $country->Code }}" readonly>
        </div>
        
               
    </form>
        <div id="feedback"></div>

    </article>
    @include('countries.readingAll') 
</main>
