<aside>
    <table class="table">
        <tr>
           <th>Select</th>
           <th>Naam</th>
           <th>Code</th>
        </tr>
        
            @foreach($countryList as $country) 
            <tr>
                <td><a href={{route('countries.show', ['id' => ($country->Id)] )}}>-></a></td>
                <td>{{ $country->Name }}</td>
                <td>{{ $country->Code }}</td>
            </tr>
        @endforeach
    </table>
</aside>