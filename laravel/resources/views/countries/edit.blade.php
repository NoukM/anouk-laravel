    <main>
    <article>
         <nav class="NavReadingOne">
            <h1>Land</h1>
            <div>
                <a href="index.php">Annuleren</a>
                <button type="submit" name="uc" value="update" form="form">Update</button>
            </div>
            </nav>
            
            
      <form  id="form" method="post" action="{{route('countries.update', $country->Id)}}">
          @method('PATCH')
          @csrf
         <div>
            <label for="Id">Ids</label>
            <input type="text" name="Id" id="Id" value={{ $country->Id }} readonly>
        </div>
        <div>
            <label for="Name">Naam</label>
            <input type="text" name="Name" id="Name" value={{ $country->Name }} >
        </div>
          <div>
            <label for="Code">Code</label>
            <input type="text" name="Code" id="Code" value={{ $country->Code }} >
        </div>
        
               
    </form>
        <div id="feedback"></div>

    </article>
     @include('countries.readingAll') 
</main>
